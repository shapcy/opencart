<?php
class ControllerStartupSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);

			// remove any empty arrays from trailing
			if (utf8_strlen(end($parts)) == 0) {
				array_pop($parts);
			}

			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");

				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);

					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}

					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}

					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}

					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}

					if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id') {
						$this->request->get['route'] = $query->row['query'];
					}
				}else
				
				// Add custom routes
				if($this->request->get['_route_'] == "cart"){
					$this->request->get['route'] = "checkout/cart";
				}else if($this->request->get['_route_'] == "checkout"){
					$this->request->get['route'] = "checkout/checkout";
				}else if($this->request->get['_route_'] == "register"){
					$this->request->get['route'] = "account/register";
				}else if($this->request->get['_route_'] == "login"){
					$this->request->get['route'] = "account/login";
				}else if($this->request->get['_route_'] == "success"){
					$this->request->get['route'] = "account/success";
				}else if($this->request->get['_route_'] == "wishlist"){
					$this->request->get['route'] = "account/wishlist";
				}else if($this->request->get['_route_'] == "profile"){
					$this->request->get['route'] = "account/account";
				}else if($this->request->get['_route_'] == "xhr/v1" || $this->request->get['_route_'] == "xhr/v1/"){
					$this->request->get['route'] = "api/v1";
				}else if($this->request->get['_route_'] == "assets/appjs"){
					$this->request->get['route'] = "common/assets/scripts";
				}else if($this->request->get['_route_'] == "assets/vendorjs"){
					$this->request->get['route'] = "common/assets/vendorjs";
				}else {
					$this->request->get['route'] = 'error/not_found';
					break;
				}
			}

			if (!isset($this->request->get['route'])) {
				if (isset($this->request->get['product_id'])) {
					$this->request->get['route'] = 'product/product';
				} elseif (isset($this->request->get['path'])) {
					$this->request->get['route'] = 'product/category';
				} elseif (isset($this->request->get['manufacturer_id'])) {
					$this->request->get['route'] = 'product/manufacturer/info';
				} elseif (isset($this->request->get['information_id'])) {
					$this->request->get['route'] = 'information/information';
				}
			}

			if (isset($this->request->get['route'])) {
				return new Action($this->request->get['route']);
			}
		}
	}

	public function rewrite($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));

		$url = '';

		$data = array();

		parse_str($url_info['query'], $data);

		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");

					if ($query->num_rows && $query->row['keyword']) {
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}
				} elseif ($key == 'path') {
					$categories = explode('_', $value);

					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");

						if ($query->num_rows && $query->row['keyword']) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';

							break;
						}
					}

					unset($data[$key]);
				}elseif($data['route'] == "checkout/cart"){	// Rewrite cart page
					$url .= '/cart';
				}elseif($data['route'] == "checkout/checkout"){ // rewrite checkout page
					$url .= '/checkout';
				}elseif($data['route'] == "account/register"){	// Rewrite register page
					$url .= '/register';
				}elseif($data['route'] == "account/login"){		// Rewrite register page
					$url .= '/login';
				}elseif($data['route'] == "account/success"){		// Rewrite register page
					$url .= '/success';
				}elseif($data['route'] == "account/wishlist"){		// Rewrite register page
					$url .= '/wishlist';
				}elseif($data['route'] == "account/account"){		// Rewrite register page
					$url .= '/profile';
				}
				
			}
		}

		if ($url) {
			unset($data['route']);

			$query = '';

			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
				}

				if ($query) {
					$query = '?' . str_replace('&', '&amp;', trim($query, '&'));
				}
			}

			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			return $link;
		}
	}
}
