<?php
class ControllerApiV1 extends Controller{
	
//	public function __construct($registry) {
//        parent::__construct($registry);
//    }
	
	public function index(){
        if(isset($this->request->post['method'])){
            $method = $this->request->post['method'];
            if(method_exists($this, $method)){
                call_user_func(array($this,$method));
            }else{
                $this->output(array('error'=>404,'message'=>'Server Error: Unknown request made!' . ': '.$method));
            }
        }
		
		if(isset($this->request->get['method'])){
            $method = $this->request->get['method'];
            if(method_exists($this, $method)){
                call_user_func(array($this,$method));
            }else{
                $this->output(array('error'=>404,'message'=>'Server Error: Unknown request made!' . ': '.$method));
            }
        }
    }
	
	private function output($data){
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }
	
	private function getProduct(){
		$this->load->model("catalog/product");
		$start = (isset($this->request->post['start']))?$this->request->post['start']:1;
		$page = (isset($this->request->post['page']))?$this->request->post['page']:1;
		$limit = (isset($this->request->post['page']))?$this->request->post['limit']:1;
		
		$category_id = ""; $filter=""; $sort = 'p.sort_order'; $order = 'ASC';
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_filter'      => $filter,
			'sort'               => $sort,
			'order'              => $order,
			'start'              => ($page - 1) * $limit,
			'limit'              => $limit
		);
		
		$this->model_catalog_product->getProducts($filter_data);
	}
	
	private function getProducts(){
		
		$this->load->model("catalog/product");
		$start = (isset($this->request->post['start']))?$this->request->post['start']:1;
		$page = (isset($this->request->post['page']))?$this->request->post['page']:1;
		$limit = (isset($this->request->post['page']))?$this->request->post['limit']:1;
		$category_id = (isset($this->request->post['category_id']))?$this->request->post['category_id']:"";
		$filter = (isset($this->request->post['filter']))?implode(',',$this->request->post['filter']):"";
		if(isset($this->request->post['sort'])){
			list($sort, $order) = explode(':', $this->request->post['sort']);
		}else{
			$sort = 'p.sort_order'; 
			$order = 'ASC';
		}
		
		$filter_data = array(
			'filter_category_id' => $category_id,
			'filter_filter'      => $filter,
			'sort'               => $sort,
			'order'              => strtoupper($order),
			'start'              => ($page - 1) * $limit,
			'limit'              => $limit
		);
		$count = $this->model_catalog_product->getTotalProducts($filter_data);
		$products = $this->model_catalog_product->getProductsQuick($filter_data);
		$products = $this->filterProductFields($products);
		
		if($products){
			$this->output(array(
				"success"	=>	true,
				"pages"		=> ceil($count/$limit),
				"next"	=>	(int)($page+1),
				"total"		=>	$count,
				"result"	=>	$products,
				"page"		=>	(int)$page,
				"limit"		=>	$limit
			));
		}else{
			$this->output(array(
				"error"	=>	true,
				"total"		=>	0,
				"message"	=>	"No products to show!"
			));
		}
	}
	
	private function getCategory(){
		
	}
	
	private function getCategories(){
		
	}
	
	private function filterProductFields($products){
		$collection = array();
		$this->load->model('tool/image');
		foreach($products as $key=>$product){
			
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			}
				
			$collection[$key]['id'] =  $product['product_id'];
			$collection[$key]['nam'] =  $product['name'];
			$collection[$key]['hrf'] =  $this->url->link('product/product', 'product_id=' . $product['product_id']);
			$collection[$key]['dsc'] =  $product['description'];
			$collection[$key]['img'] =  $image;
			$collection[$key]['prc'] =  $product['price'];
			$collection[$key]['spc'] =  $product['special'];
		}
		return $collection;
	}
	
	private function getContent(){
		$this->load->model('api/api');
		$json = array("status"=>true);
		$json['result']['aside'] = $this->model_api_api->_loadModules(1);
		$json['result']['top'] = $this->model_api_api->_loadModules(1,'content_top');
		$json['result']['bottom'] = $this->model_api_api->_loadModules(1,'content_bottom');
		$this->output($json);
	}
	
	
	
	private function router(){
		$this->load->model('api/api');
		if(isset($this->request->post['_r']) && isset($this->request->post['_r'][0])){
			$path = $this->request->post['_r'][0];
			$parts = explode("/", $path);
			$query = end($parts);
			$routeContent = $this->model_api_api->findRoute($query);
			if($routeContent){
				$this->output($routeContent);
			}else{
				$this->output(array('error'=>true,"mes"=>"Nothing found..."));
			}
			
		}
	}
	
	public function autocomplete() {
        $json = array();
        if (isset($this->request->get['store_id'])) {
            $store_id = $this->request->get['store_id'];
        } else {
            $store_id = 0;
        }
        $showthumb = true;
        $showprice = true;
        $showextra = true;

        $limit = 6;
        $wexsmalldevice = true;    
        $wsmalldevice = true;
        $wmediumdevice = true;
        $wlargedevice = true;

        $lmexsmalldevice = 0;    
        $lmsmalldevice = 0;
        $lmmediumdevice = 0;
        $lmlargedevice = 0;
        $thumbwidth = 40;
        $thumbheight = 40;
        $extrawidth = 0;
        $minchar = 1;

        $extrafield = false;
        
        $searchfor = array('name','model');

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/product');

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            
            $data = array(
                'filter_name' => $filter_name,
                'start' => 0,
                'limit' => $limit,
                'filter_description' => (in_array('description', $searchfor) ? true : false),
                'filter_tag' => (in_array('tag', $searchfor) ? $filter_name : false),
                'filter_model' => (in_array('model', $searchfor) ? true : false),
                'filter_sku' => (in_array('sku', $searchfor) ? true : false),
                'filter_upc' => (in_array('upc', $searchfor) ? true : false),
                'filter_ean' => (in_array('ean', $searchfor) ? true : false),
                'filter_jan' => (in_array('jan', $searchfor) ? true : false),
                'filter_isbn' => (in_array('isbn', $searchfor) ? true : false),
                'filter_mpn' => (in_array('mpn', $searchfor) ? true : false),
                'filter_manufacturer' => (in_array('manufacturer', $searchfor) ? true : false),
            );

            
			$results = $this->model_catalog_product->getProducts($data);
            
            $this->load->model('tool/image');
            foreach ($results as $result) {
                if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                    $image = $this->model_tool_image->resize($result['image'], $thumbwidth, $thumbheight);
                } else {
                    $image = $this->model_tool_image->resize('no_image.jpg', $thumbwidth, $thumbheight);
                }
				$image = $result['image'];
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                   // $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float) $result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
//                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                $json[] = array(
                    'id' => $result['product_id'],
                    'data' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                    'price' => $price,
                    'special' => $special,
                    'thumbnail' => $image,
                    
                );
            }
        }

        $this->response->setOutput(json_encode($json));
        //return 'sss';
    }
	
}