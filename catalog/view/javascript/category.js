var Service = {
	options:{
		url:'http://localhost/lebanse-v1/xhr/v1',
		tpl:'http://localhost/lebanse-v1/catalog/view/javascript/tpl/'
	},
	defaults:{},
	view:function(name){
		if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
			$.ajax({
				url : Service.options.tpl + name + '.htm' ,
				success : function(data) {
					if (Handlebars.templates === undefined) {
						Handlebars.templates = {};
					}
					Handlebars.templates[name] = Handlebars.compile(data);
				},
				async : false
			});
		}
		return Handlebars.templates[name];
	},
	fetch:function(params,callback){
		var postdata = $.extend()
		$.ajax({
			url:Service.options.url,
			method:'POST',
			dataType:'json',
			data:$.extend(Service.defaults,params),
			beforeSend: function( xhr ) {
			
			},
			error:function(e){
				console.log(e);
			},
			success:function(r){
				callback(r);
			}
		});
	}
}

var Catalog = {
	products:null,
	product:null,
	getCetgory:function(){
		
	},
	getCategories:function(){
		
	},
	getProduct:function(){
		
	},
	getProducts:function(params,callback){
		var defauts = {method:'getProducts',limit:20,page:1};
		var params = $.extend(defauts,params);
		Service.fetch(params,callback);
	},
	Template:{
		loadProducts:function(params){ console.log('called');
//			Catalog.getProducts(params);
		}
	}
}

;var Category = function(id){
	this.id = id;
	this.container = $("_contents");
	this.filters = [];
	this.infst = true;
	return this;
};

Category.prototype = {
	setContent:function(html,append){
		
	},
	scroll:function(){
		var d=this;
		   $(window).scroll(function(){
				if(d.infst && d.page<d.pages){
//					console.log(d)
					var s=$(window).scrollTop();var h=$(document).height();var w=$(window).height();
					var f=h-w-650;
					   if  (s >= f){
							d.infst=false;
							d.filterList({page:d.next},true);
						}
				 }   
			});
	  },
	createList: function(){
		$this = this;
		Catalog.getProducts({category_id:this.id,filter:this.filters},function(data){
			var list = Service.view('list');
			$this.pages = data.pages;
			$this.page	=	data.page;
			
			var html = list({products:data.result});
			$("#content .collection").html(html);
			$this.next = data.next;
			$this.scroll();
		});
		return this;
	},
	filterList: function(options,append){
		$this = this;
		p = (options.page)?options.page:1;
		Catalog.getProducts({category_id:this.id,sort:this.sort,filter:this.filters,page:p},function(data){
			var list = Service.view('list');
			$this.pages = data.pages;
			$this.page	=	data.page;
			$this.next = data.next;
			var html = "";
			if(append) html = '<div class="bar tc"><h5>@showing page '+data.page+' of total '+data.pages+' pages</h5><hr/></div>';
			html += list({products:data.result});
			(append)?$("#content .collection").append(html):$("#content .collection").html(html);
			$this.infst=true;
		});
//		$this.scroll();
		return this;
	},
	prepare:function(){
		$this = this;
		$this.filters = [];
		$("input[name='filter[]']:checked").each(function(){
			$this.filters.push($(this).val());
		});
		$this.sort = $("._sort li.active").find('a').attr('id');
		return this;
	},
	filter:function(){
		$this=this;
		$("._f").change(function(e){
			e.preventDefault();
			$this.prepare().filterList({page:1});
		});
		
		$("._sort a").click(function(e){ e.preventDefault();
			if(!$(this).parent().hasClass('active')){
				$("._sort li").removeClass('active');
				$(this).parent().addClass('active');
				$this.page = 1;
				$this.prepare().filterList({page:1});
			}
		});
		return this;
	},
	doFilter:function(){
		
	}
}

var listing 
		= new Category($("#content").data('category')).prepare().createList().filter();