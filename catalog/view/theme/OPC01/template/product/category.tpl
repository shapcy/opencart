<?php echo $header; ?>
<div class="container product-category">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	  <!-- Category Description START -->
      <h1 class="category-name"><?php echo $heading_title; ?></h1>
     
	  <div id="filter">
		<ul class="nav nav-tabs _sort">
			  <li><a id="p.view:desc" href="#"><i class="fa fa-asterisk" aria-hidden="true"></i> popular</a></li>
			  <li><a id="p.price:asc" href="#"><i class="fa fa-sort-asc fa-6" aria-hidden="true"></i> Low Price</a></li>
			  <li><a id="p.price:desc" href="#"><i class="fa fa-sort-desc" aria-hidden="true"></i> High Price</a></li>
			  <li><a id="pd.name:asc" href="#"><i class="fa fa-sort-alpha-asc" aria-hidden="true"></i> sort asc</a></li>
			  <li><a id="pd.name:desc" href="#"><i class="fa fa-sort-alpha-desc" aria-hidden="true"></i> sort desc</a></li>					
			  <li><a id="p.product_id:desc" href="#"><i class="fa fa-link" aria-hidden="true"></i> New</a></li>
		</ul>
		<div class="tab-content">
			<div id="popular" class="tab-pane fade in active"></div>
			<div id="low-price" class="tab-pane fade"></div>
			<div id="high-price" class="tab-pane fade"></div>
			<div id="new" class="tab-pane fade"></div>
			<div id="compare-total" class="tab-pane fade"></div>
		</div>
	</div>

	 

      <?php if ($products) { ?>

	  <!-- Category products START -->
	  <div class="category-products">
		<div class="row collection">
      </div>
	  </div>
	  <!-- Category products END -->

	 

      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script src="catalog/view/javascript/category.js" type="text/javascript"></script>
<?php echo $footer; ?>
